package com.aardwark.kw.item.service;

public class ItemTypeServiceException extends RuntimeException {

    private final Code errorCode;

    public ItemTypeServiceException(Code errorCode) {
        this.errorCode = errorCode;
    }

    public ItemTypeServiceException(Code errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ItemTypeServiceException(Code errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ItemTypeServiceException(Code errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ItemTypeServiceException(Code errorCode,
                                    String message,
                                    Throwable cause,
                                    boolean enableSuppression,
                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public enum Code {
        NO_ENTITY_FOUND,
        UNACCEPTABLE_INPUT
    }

    public Code getErrorCode() {
        return errorCode;
    }
}
