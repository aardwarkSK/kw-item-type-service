package com.aardwark.kw.item.service;

import com.aardwark.kw.item.service.persistence.entity.ItemType;
import com.aardwark.kw.item.service.persistence.entity.Status;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ItemTypeService {

    Optional<ItemType> findById(final UUID id);

    ItemType getById(final UUID id);

    List<ItemType> findAll();

    List<ItemType> findByStatus(final Status status);

    ItemType create(final ItemType entity);

    boolean setStatus(final UUID uuid, final Status status);
}
