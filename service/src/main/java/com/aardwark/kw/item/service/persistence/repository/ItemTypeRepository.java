package com.aardwark.kw.item.service.persistence.repository;

import com.aardwark.kw.item.service.persistence.entity.ItemType;
import com.aardwark.kw.item.service.persistence.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ItemTypeRepository extends JpaRepository<ItemType, UUID> {
    List<ItemType> findByStatus(Status status);
}
