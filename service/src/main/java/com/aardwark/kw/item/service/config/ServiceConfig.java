package com.aardwark.kw.item.service.config;

import com.aardwark.kw.item.service.ItemTypeService;
import com.aardwark.kw.item.service.impl.ItemTypeServiceImpl;
import com.aardwark.kw.item.service.persistence.repository.ItemTypeRepository;
import org.springframework.context.annotation.Bean;

public class ServiceConfig {
    @Bean
    public ItemTypeService initItemTypeService(ItemTypeRepository repository) {
        return new ItemTypeServiceImpl(repository);
    }
}
