package com.aardwark.kw.item.service.persistence.entity;

public enum Status {
    NEW,
    ACTIVE,
    DEACTIVATED,
    DELETED
}
