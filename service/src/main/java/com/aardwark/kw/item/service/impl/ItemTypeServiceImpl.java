package com.aardwark.kw.item.service.impl;

import com.aardwark.kw.item.service.ItemTypeService;
import com.aardwark.kw.item.service.ItemTypeServiceException;
import com.aardwark.kw.item.service.ItemTypeServiceException.Code;
import com.aardwark.kw.item.service.persistence.entity.ItemType;
import com.aardwark.kw.item.service.persistence.entity.Status;
import com.aardwark.kw.item.service.persistence.repository.ItemTypeRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static java.text.MessageFormat.format;

public class ItemTypeServiceImpl implements ItemTypeService {

    private final ItemTypeRepository repository;

    public ItemTypeServiceImpl(ItemTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<ItemType> findById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public ItemType getById(UUID id) {
        return findById(id).orElseThrow(() -> constructNotFoundException(id));
    }

    @Override
    public List<ItemType> findAll() {
        return repository.findAll();
    }

    @Override
    public List<ItemType> findByStatus(final Status status) {
        return repository.findByStatus(status);
    }

    @Override
    public ItemType create(final ItemType entity) {
        entity.setStatus(Status.ACTIVE);
        entity.setCreationDate(LocalDateTime.now());
        entity.setId(UUID.randomUUID());

        return repository.save(entity);
    }

    @Override
    public boolean setStatus(final UUID id, final Status status) {
        Objects.requireNonNull(id, "ItemType id is expected to be not null");
        Objects.requireNonNull(id, "new ItemType Status is expected to be not null");

        final ItemType itemType = getById(id);

        boolean result = status.equals(itemType.getStatus());
        itemType.setStatus(status);
        repository.save(itemType);
        return result;
    }

    private ItemTypeServiceException constructNotFoundException(UUID id) {
        return new ItemTypeServiceException(Code.NO_ENTITY_FOUND,
                                            format("Item Type with id {0}' has not been been found",
                                                   id));
    }

}
