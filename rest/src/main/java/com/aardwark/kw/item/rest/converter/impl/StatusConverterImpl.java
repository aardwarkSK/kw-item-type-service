package com.aardwark.kw.item.rest.converter.impl;

import static java.text.MessageFormat.format;

import com.aardwark.kw.item.rest.converter.StatusConverter;
import com.aardwark.kw.item.service.persistence.entity.Status;

public class StatusConverterImpl implements StatusConverter {

    @Override
    public Status convert(com.aardwark.kw.item.api.rest.model.Status input) {
        if (null == input) {
            return null;
        }
        switch (input) {
            case ACTIVE:
                return Status.ACTIVE;
            case DEACTIVATED:
                return Status.DEACTIVATED;
            case NEW:
                return Status.NEW;
            case DELETED:
                return Status.DELETED;
            default:
                throw new IllegalArgumentException(format("Status {0} is not support",
                                                          input.name()));
        }
    }

    @Override
    public com.aardwark.kw.item.api.rest.model.Status convert(Status input) {
        if (null == input) {
            return null;
        }
        switch (input) {
            case ACTIVE:
                return com.aardwark.kw.item.api.rest.model.Status.ACTIVE;
            case DEACTIVATED:
                return com.aardwark.kw.item.api.rest.model.Status.DEACTIVATED;
            case NEW:
                return com.aardwark.kw.item.api.rest.model.Status.NEW;
            case DELETED:
                return com.aardwark.kw.item.api.rest.model.Status.DELETED;
            default:
                throw new IllegalArgumentException(format("Status {0} is not support",
                                                          input.name()));
        }
    }
}
