package com.aardwark.kw.item.rest;

import com.aardwark.kw.item.service.ItemTypeServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ItemTypeExceptionHandler {

    @ExceptionHandler(ItemTypeServiceException.class)
    public ResponseEntity<String> handleOrdersLogicException(final ItemTypeServiceException itemTypeServiceException) {
        HttpStatus status = mapErrorCodeToHttpStatus(itemTypeServiceException.getErrorCode());

        return ResponseEntity.status(status).body(itemTypeServiceException.getMessage());

    }

    private HttpStatus mapErrorCodeToHttpStatus(ItemTypeServiceException.Code errorCode) {
        switch (errorCode) {
            case NO_ENTITY_FOUND:
                return HttpStatus.NOT_FOUND;
            case UNACCEPTABLE_INPUT:
                return HttpStatus.BAD_REQUEST;
            default:
                return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
