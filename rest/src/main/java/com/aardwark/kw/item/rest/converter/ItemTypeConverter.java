package com.aardwark.kw.item.rest.converter;

import com.aardwark.kw.item.service.persistence.entity.ItemType;

public interface ItemTypeConverter {

    ItemType convert(com.aardwark.kw.item.api.rest.model.ItemType rest);

    ItemType convert(com.aardwark.kw.item.api.rest.model.NewItemType rest);

    com.aardwark.kw.item.api.rest.model.ItemType convert(ItemType entity);

}
