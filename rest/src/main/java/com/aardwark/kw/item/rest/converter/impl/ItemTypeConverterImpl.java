package com.aardwark.kw.item.rest.converter.impl;

import com.aardwark.kw.item.rest.converter.ItemTypeConverter;
import com.aardwark.kw.item.rest.converter.StatusConverter;
import com.aardwark.kw.item.service.persistence.entity.ItemType;
import com.aardwark.kw.item.api.rest.model.NewItemType;

public class ItemTypeConverterImpl implements ItemTypeConverter {

    private final StatusConverter statusConverter;

    public ItemTypeConverterImpl(StatusConverter statusConverter) {
        this.statusConverter = statusConverter;
    }

    @Override
    public ItemType convert(NewItemType input) {
        if (null == input) {
            return null;
        }
        ItemType result = new ItemType();
        result.setName(input.getName());
        return result;
    }

    @Override
    public ItemType convert(com.aardwark.kw.item.api.rest.model.ItemType input) {
        if (null == input) {
            return null;
        }
        ItemType result = new ItemType();
        result.setCreationDate(input.getCreationDate());
        result.setName(input.getName());
        result.setStatus(statusConverter.convert(input.getStatus()));
        return result;
    }

    @Override
    public com.aardwark.kw.item.api.rest.model.ItemType convert(ItemType input) {
        if (null == input) {
            return null;
        }
        com.aardwark.kw.item.api.rest.model.ItemType result = new com.aardwark.kw.item.api.rest.model.ItemType();
        result.setId(input.getId());
        result.setCreationDate(input.getCreationDate());
        result.setName(input.getName());
        result.setStatus(statusConverter.convert(input.getStatus()));
        return result;
    }
}
