package com.aardwark.kw.item.rest.config;

import com.aardwark.kw.item.rest.converter.ItemTypeConverter;
import com.aardwark.kw.item.rest.converter.StatusConverter;
import com.aardwark.kw.item.rest.converter.impl.ItemTypeConverterImpl;
import com.aardwark.kw.item.rest.converter.impl.StatusConverterImpl;
import com.aardwark.kw.item.service.config.ServiceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import({ServiceConfig.class})
@ComponentScan("com.aardwark.kw.item.rest")
public class RestConfig {

    @Bean
    public StatusConverter initStatusConverter() {
        return new StatusConverterImpl();
    }

    @Bean
    public ItemTypeConverter initItemTypeConverter(final StatusConverter statusConverter) {
        return new ItemTypeConverterImpl(statusConverter);
    }
}
