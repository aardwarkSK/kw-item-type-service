package com.aardwark.kw.item.rest.converter;

import com.aardwark.kw.item.service.persistence.entity.Status;

public interface StatusConverter {

    Status convert(com.aardwark.kw.item.api.rest.model.Status input);

    com.aardwark.kw.item.api.rest.model.Status convert(Status input);

}
