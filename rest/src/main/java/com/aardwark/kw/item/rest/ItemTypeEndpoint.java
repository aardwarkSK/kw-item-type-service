package com.aardwark.kw.item.rest;

import com.aardwark.kw.item.rest.converter.ItemTypeConverter;
import com.aardwark.kw.item.rest.converter.StatusConverter;
import com.aardwark.kw.item.service.ItemTypeService;
import com.aardwark.kw.item.api.rest.model.ItemType;
import com.aardwark.kw.item.api.rest.model.NewItemType;
import com.aardwark.kw.item.api.rest.model.Status;
import com.aardwark.kw.item.api.rest.service.ItemsApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class ItemTypeEndpoint implements ItemsApi {

    private final ItemTypeConverter itemTypeConverter;
    private final ItemTypeService itemTypeService;
    private final StatusConverter statusConverter;

    public ItemTypeEndpoint(ItemTypeConverter itemTypeConverter,
                            ItemTypeService itemTypeService,
                            StatusConverter statusConverter) {
        this.itemTypeConverter = itemTypeConverter;
        this.itemTypeService = itemTypeService;
        this.statusConverter = statusConverter;
    }

    @Override
    public ResponseEntity<List<ItemType>> getItemTypes() {
        final List<ItemType> result = itemTypeService.findAll()
                                                     .stream()
                                                     .map(itemTypeConverter::convert)
                                                     .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<ItemType>> itemsFilterGet(Status status) {
        final List<ItemType> result =
            itemTypeService.findByStatus(statusConverter.convert(status))
                           .stream()
                           .map(itemTypeConverter::convert)
                           .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<ItemType> getItemType(UUID id) {
        return ResponseEntity.ok(itemTypeConverter.convert(itemTypeService.getById(id)));
    }

    @Override
    public ResponseEntity<Void> updateItemTypeStatus(UUID id, Status status) {
        itemTypeService.setStatus(id, statusConverter.convert(status));

        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<ItemType> createItemType(NewItemType itemType) {
        com.aardwark.kw.item.service.persistence.entity.ItemType entity = itemTypeConverter.convert(itemType);

        ItemType createdItemType = itemTypeConverter.convert(itemTypeService.create(entity));
        return ResponseEntity.ok(createdItemType);
    }

}
