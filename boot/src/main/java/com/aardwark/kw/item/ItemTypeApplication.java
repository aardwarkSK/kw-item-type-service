package com.aardwark.kw.item;

import com.aardwark.kw.item.config.PersistenceConfig;
import com.aardwark.kw.item.config.WebConfig;
import com.aardwark.kw.item.rest.config.RestConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;

@Import({
        PersistenceConfig.class,
        WebConfig.class,
        RestConfig.class,
})
public class ItemTypeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItemTypeApplication.class, args);
    }

}
